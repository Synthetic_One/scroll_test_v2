﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Thumbnail : MonoBehaviour
{
	[SerializeField]
	private Image m_image;
	[SerializeField]
	private Text m_text;

	private ThumbnailVO _thumbnailVO;

	public ThumbnailVO thumbnailVO
	{
		get
		{
			return _thumbnailVO;
		}
		set
		{
			_thumbnailVO = value;
			m_image.sprite = SpriteManager.instance.GetSprite(_thumbnailVO.id);
			m_text.text = _thumbnailVO.id.ToString();
		}
	}

	private void Awake()
	{
		if (!m_image)
			m_image = GetComponent<Image>();

		if (!m_text)
			m_text = GetComponentInChildren<Text>();
	}

	public void OnClick()
	{
		Debug.Log("Thumbnail clicked: " + _thumbnailVO.id);
	}
}