﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//allows to avoid ContentSizeFitter that dirties the canvas every frame, causing rebuild
public class ExpandingGridLayoutGroup : GridLayoutGroup
{
	private void Update()
	{
		rectTransform.sizeDelta = new Vector2(GetTotalPreferredSize(0), GetTotalPreferredSize(1));
	}
}