﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Simplified scroll rect that only supports vertical scrolling
/// </summary>
public class CustomScrollRect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IScrollHandler
{
	[SerializeField]
	private RectTransform m_viewport;
	[SerializeField]
	private RectTransform m_content;

	[SerializeField]
	private float m_deceleration = 0.135f;
	[SerializeField]
	private float m_scrollSensitivity;
	public UnityEngine.UI.ScrollRect.ScrollRectEvent onValueChanged;

	private RectTransform m_canvasRect;
	private Rect m_viewportOld,
		m_contentOld;

	private List<Vector2> m_dragCoordinates = new List<Vector2>();
	private List<float> m_offsets = new List<float>();
	private int m_offsetsAveraged = 4;
	private float m_offset;
	private float m_velocity = 0;
	private bool m_changesMade = false;

	public float verticalNormalizedPosition
	{
		get
		{
			var sizeDelta = CalculateDeltaSize();
			if (sizeDelta == 0)
				return 0;

			return 1 - m_content.transform.localPosition.y / sizeDelta;
		}
		set
		{
			var prevVerticalNormalizedPosition = verticalNormalizedPosition;
			var clampedVerticalNormalizedPosition = Mathf.Clamp01(value);
			var maxY = CalculateDeltaSize();
			m_content.transform.localPosition = new Vector3(m_content.transform.localPosition.x, Mathf.Max(0, (1 - clampedVerticalNormalizedPosition) * maxY), m_content.transform.localPosition.z);
			var newVerticalNormalizedPosition = verticalNormalizedPosition;
			if (prevVerticalNormalizedPosition != newVerticalNormalizedPosition)
				onValueChanged.Invoke(new Vector2(0f, newVerticalNormalizedPosition));
		}
	}

	#region Mono

	private void Awake()
	{
		m_canvasRect = transform.root.GetComponent<RectTransform>();
	}


	private void LateUpdate()
	{
		if (m_viewport.rect != m_viewportOld)
		{
			m_changesMade = true;
			m_viewportOld = new Rect(m_viewport.rect);
		}
		if (m_content.rect != m_contentOld)
		{
			m_changesMade = true;
			m_contentOld = new Rect(m_content.rect);
		}
		if (m_velocity != 0)
		{
			m_changesMade = true;
			m_velocity = (m_velocity / Mathf.Abs(m_velocity)) * Mathf.FloorToInt(Mathf.Abs(m_velocity) * (1 - m_deceleration));
			m_offset = m_velocity;
		}
		if (m_changesMade)
		{
			OffsetContent(m_offset);
			m_changesMade = false;
			m_offset = 0;
		}
	}


	#endregion

	#region Interfaces

	void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
	{
		m_velocity = 0;
		m_dragCoordinates.Clear();
		m_offsets.Clear();
		m_dragCoordinates.Add(ConvertEventDataDrag(eventData));
	}

	void IScrollHandler.OnScroll(PointerEventData eventData)
	{
		UpdateOffsetsScroll(ConvertEventDataScroll(eventData));
		OffsetContent(m_offsets[m_offsets.Count - 1]);
	}

	void IDragHandler.OnDrag(PointerEventData eventData)
	{
		m_dragCoordinates.Add(ConvertEventDataDrag(eventData));
		UpdateOffsetsDrag();
		OffsetContent(m_offsets[m_offsets.Count - 1]);
	}

	void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
	{
		m_dragCoordinates.Add(ConvertEventDataDrag(eventData));
		UpdateOffsetsDrag();
		OffsetContent(m_offsets[m_offsets.Count - 1]);

		var totalOffsets = m_offsets.Sum();
		m_velocity = totalOffsets / m_offsetsAveraged;
		m_dragCoordinates.Clear();
		m_offsets.Clear();
	}

	#endregion

	private float CalculateDeltaSize()
	{
		return Mathf.Max(0, m_content.rect.height - m_viewport.rect.height); ;
	}

	private Vector2 ConvertEventDataDrag(PointerEventData eventData)
	{
		return new Vector2(eventData.position.x / eventData.pressEventCamera.pixelWidth * m_canvasRect.rect.width, eventData.position.y / eventData.pressEventCamera.pixelHeight * m_canvasRect.rect.height);
	}

	private Vector2 ConvertEventDataScroll(PointerEventData eventData)
	{
		//use negative scrollDelta, as Down is passed as positive in the event
		return new Vector2(eventData.scrollDelta.x / eventData.enterEventCamera.pixelWidth * m_canvasRect.rect.width, -eventData.scrollDelta.y / eventData.enterEventCamera.pixelHeight * m_canvasRect.rect.height) * m_scrollSensitivity;
	}

	private void OffsetContent(float givenOffset)
	{
		var newY = Mathf.Max(0, Mathf.Min(CalculateDeltaSize(), m_content.transform.localPosition.y + givenOffset));
		if (m_content.transform.localPosition.y != newY)
			m_content.transform.localPosition = new Vector3(m_content.transform.localPosition.x, newY, m_content.transform.localPosition.z);
		onValueChanged.Invoke(new Vector2(0f, verticalNormalizedPosition));
	}

	private void UpdateOffsetsDrag()
	{
		m_offsets.Add(m_dragCoordinates[m_dragCoordinates.Count - 1].y - m_dragCoordinates[m_dragCoordinates.Count - 2].y);
		if (m_offsets.Count > m_offsetsAveraged)
			m_offsets.RemoveAt(0);
	}

	private void UpdateOffsetsScroll(Vector2 givenScrollDelta)
	{
		m_offsets.Add(givenScrollDelta.y);
		if (m_offsets.Count > m_offsetsAveraged)
			m_offsets.RemoveAt(0);
	}
}