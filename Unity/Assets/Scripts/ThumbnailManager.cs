﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ThumbnailManager : MonoBehaviour
{
	public RectTransform container;
	public Thumbnail prefab;

	public CustomScrollRect scrollRect;
	public RectTransform viewport;
	public GridLayoutGroup grid;

	private Vector2Int gridOnScreenSize;
	private int poolSize;
	private int poolPointer = 0;
	private float lastScrollPos;

	private const int EXTRA_ROWS_EACH_SIDE = 2;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
	private List<Thumbnail> pool = new List<Thumbnail>();

	private void Start()
	{
		lastScrollPos = 1f;
		gridOnScreenSize = new Vector2Int(Mathf.CeilToInt(viewport.rect.width / (grid.cellSize.x + grid.spacing.x)), Mathf.CeilToInt(viewport.rect.height / (grid.cellSize.y + grid.spacing.y)));

		createThumbnailVOList();

		BuildPool();
	}

	public void OnScrolled()
	{
		var vertScrollPos = scrollRect.verticalNormalizedPosition;

		//get scrolling direction while not turning 0 into 1, like Mathf.Sign does
		var change = vertScrollPos - lastScrollPos;
		var direction = Mathf.Approximately(0f, change) ? 0f : Mathf.Sign(change);
		lastScrollPos = vertScrollPos;

		var rowHeight = grid.cellSize.y + grid.spacing.y;

		if (direction < 0//scrolling down
			&& container.anchoredPosition.y > rowHeight * EXTRA_ROWS_EACH_SIDE//and the content rect has more than one row's height that we can cull and reuse at the bottom
			&& poolPointer < _thumbnailVOList.Count - poolSize)//and we're not at the end of the data set
		{
			//take the top [RowLength] elements and push them to the bottom of both the pool and content's children list, while giving them new values
			for (var i = 0; i < gridOnScreenSize.x; i++)
			{
				var element = pool[0];
				element.transform.SetSiblingIndex(poolSize);

				var withinBounds = _thumbnailVOList.Count > poolPointer + poolSize + i;
				if(withinBounds)
					element.thumbnailVO = _thumbnailVOList[poolPointer + poolSize + i];
				element.gameObject.SetActive(withinBounds);

				pool.RemoveAt(0);
				pool.Add(element);
			}

			//update which data entry the first element of the pool points at after scrolling
			poolPointer += gridOnScreenSize.x;

			//move the content rect one [RowHeight] down
			container.anchoredPosition = new Vector2(container.anchoredPosition.x, container.anchoredPosition.y - rowHeight);
			lastScrollPos = scrollRect.verticalNormalizedPosition;
		}
		else if (direction > 0//scrolling up
			&& container.anchoredPosition.y < rowHeight * EXTRA_ROWS_EACH_SIDE//and the content rect has less than one row's height at the top, so we need to extend this space
			&& poolPointer > 0)//and we're not at the beginning of the data set
		{
			//take the bottom [RowLength] elements and insert them at the beginning of both the pool and content's children list, while giving them new values
			for (var i = 0; i < gridOnScreenSize.x; i++)
			{
				var element = pool[poolSize - 1];
				element.transform.SetSiblingIndex(0);
				element.thumbnailVO = _thumbnailVOList[poolPointer - (i + 1)];
				pool.RemoveAt(poolSize - 1);
				pool.Insert(0, element);
			}

			//update which data entry the first element of the pool points at after scrolling
			poolPointer -= gridOnScreenSize.x;

			//move the content rect one [RowHeight] up
			container.anchoredPosition = new Vector2(container.anchoredPosition.x, container.anchoredPosition.y + rowHeight);
			lastScrollPos = scrollRect.verticalNormalizedPosition;
		}
	}

	private void BuildPool()
	{
		poolSize = Mathf.Min(_thumbnailVOList.Count, gridOnScreenSize.x * gridOnScreenSize.y + EXTRA_ROWS_EACH_SIDE * gridOnScreenSize.x * 2);

		Thumbnail gameObj;
		for (var i = 0; i < poolSize; i++)
		{
			gameObj = Instantiate(prefab, container, false);
			gameObj.thumbnailVO = _thumbnailVOList[i];
			pool.Add(gameObj);
		}
	}

	private void createThumbnailVOList()
	{
		ThumbnailVO thumbnailVO;
		for (int i = 0; i < 1000; i++)
		{
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
			_thumbnailVOList.Add(thumbnailVO);
		}
	}
}
